import { GraphQLClient } from 'graphql-request'
import config from '../../config/config'

const endpoint: string = config.serverURL
const serverToken: string = config.serverToken

export async function createGraphQLClient (): Promise<GraphQLClient> {
  return new GraphQLClient(endpoint, {
    headers: {
      Authorization: serverToken
    }
  })
}
