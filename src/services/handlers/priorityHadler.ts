import { type Context } from 'telegraf'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from './checkForChatId'
import { priorityButtons } from '../../buttons/markups'

export async function setPriority (ctx: Context): Promise<void> {
  if (isChatIdAllowed(ctx)) {
    ctx.editMessageText(i18next.t('priority_set'), priorityButtons())
  } else {
    i18next.t('errorChatId')
  }
}
