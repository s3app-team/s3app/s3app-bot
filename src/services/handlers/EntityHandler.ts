import { type Context } from 'telegraf'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from './checkForChatId'
import { gradeTensionButtons, gradeDriverButtons, setEntitiesMenuButtons, stateDriverButtons, statusProposalButtons, cancelButton } from '../../buttons/markups'

export function setEntities (ctx: Context, flag: string): void {
  const newLine: string = '\n'
  console.log(flag)
  if (isChatIdAllowed(ctx)) {
    if (flag === 'choice') {
      const username: string | undefined = ctx.from?.username
      ctx.editMessageText(i18next.t('setMenu', { username, newLine }), setEntitiesMenuButtons())
    }
    if (flag === 'add_issue') {
      ctx.editMessageText(i18next.t('enterIssue'), cancelButton())
    }
    if (flag === 'add_event') {
      ctx.editMessageText(i18next.t('enterEvent'), cancelButton())
    }
    if (flag === 'add_event_command') {
      ctx.reply(i18next.t('startDate'), cancelButton())
    }
    if (flag === 'startDate') {
      ctx.reply(i18next.t('startDate'), cancelButton())
    }
    if (flag === 'startDateAI') {
      ctx.reply(i18next.t('startDateAI'), cancelButton())
    }
    if (flag === 'add_thought') {
      ctx.editMessageText(i18next.t('enterThought'), cancelButton())
    }
    if (flag === 'add_tension') {
      ctx.editMessageText(i18next.t('enterTension'), cancelButton())
    }
    if (flag === 'tensionMark') {
      ctx.reply(i18next.t('toGrade'), gradeTensionButtons())
    }
    if (flag === 'description') {
      ctx.reply(i18next.t('description'), cancelButton())
    }
    if (flag === 'add_driver') {
      ctx.editMessageText(i18next.t('enterDriver'), cancelButton())
    }
    if (flag === 'driverEffect') {
      ctx.reply(i18next.t('toGrade'), gradeDriverButtons())
    }
    if (flag === 'driverState') {
      ctx.editMessageText(i18next.t('setState'), stateDriverButtons())
    }
    if (flag === 'add_proposal') {
      ctx.editMessageText(i18next.t('enterProposal'), cancelButton())
    }
    if (flag === 'status') {
      ctx.reply(i18next.t('setStatus'), statusProposalButtons())
    }
    if (flag === 'add_ai_request') {
      ctx.editMessageText(i18next.t('setAIRequest'), cancelButton())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }))
  }
}
