import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSessionProposal } from '../../types/proposals'
import config from '../../config/config'
import { Menu } from '../../buttons/markups'

export async function setProposal (ctx: Context, userSessionsProposal: UserSessionProposal): Promise<void> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
        mutation($proposal: ProposalInput!){
            createProposal(proposal: $proposal) {
                id
                name
                description
                status
                domain {
                    id
                }
                drivers{
                    id
                }
            }
        }`
    const variables = {
      proposal: {
        name: userSessionsProposal.proposalName,
        description: userSessionsProposal.description,
        status: userSessionsProposal.status,
        driverIds: userSessionsProposal.driverIds,
        domainId: userSessionsProposal.domainId
      }
    }
    const data: { createProposal?: { id: string, name: string, description: string, status: string, driverIds: { id: string }, domain: { id: string } } } = await client.request(query, variables)
    const proposal = data.createProposal
    const proposalName = proposal?.name
    const proposalId: string = proposal?.id ?? ''
    const domainId: string = proposal?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/proposals/' + proposalId
    const linkName = `${i18next.t('proposalCreated')} <a href="${link}">${proposalName}</a>`
    console.log(`Hello----->\n${link}\n${domainId}\n${proposalId}`)
    ctx.editMessageText(linkName, {
      link_preview_options: {
        is_disabled: true
      },
      parse_mode: 'HTML',
      ...Menu()
    })
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}
