import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSessionEvent } from '../../types/events'
import config from '../../config/config'
import { Menu } from '../../buttons/markups'
import parser from 'any-date-parser'
import { AIswitcher } from '../../bot_api'

export async function setEvent (ctx: Context, userSessionsEvent: UserSessionEvent): Promise<void> {
  try {
    const locale = 'ru'
    const startDateComponents = parser.attempt(userSessionsEvent.startDate, locale)
    console.log('startDateComponents.year-------------->', startDateComponents.year)

    if (!startDateComponents.year || !startDateComponents.month || !startDateComponents.day) {
      throw new Error('Invalid start date components')
    }

    const parsedStartDate = new Date(
      startDateComponents.year as number,
      startDateComponents.month - 1,
      startDateComponents.day as number,
      startDateComponents.hour as number || 0,
      startDateComponents.minute as number || 0
    )

    const parsedEndDate = new Date(parsedStartDate)
    parsedEndDate.setHours(parsedEndDate.getHours() + 2)

    const formattedStartDate = parsedStartDate.toISOString()
    const formattedEndDateWithOffset = parsedEndDate.toISOString()

    const client = await createGraphQLClient()
    const query: string = gql`
     mutation($event: EventInput!) {
        createEvent(event: $event) {
         name
         startDate
         endDate
         domain {
            id
         }
        }
     }
    `
    const variables = {
      event: {
        name: userSessionsEvent.eventName,
        startDate: formattedStartDate,
        endDate: formattedEndDateWithOffset,
        domainId: userSessionsEvent.domainId
      }
    }
    const data: { createEvent?: { name: string, startDate: string, endDate: string, domain: { id: string } } } = await client.request(query, variables)
    const event = data.createEvent
    const eventName = event?.name
    const startDate = new Date(event?.startDate ?? '').toLocaleDateString('ru-RU', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit'
    })
    const endDate = new Date(event?.endDate ?? '').toLocaleDateString('ru-RU', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit'
    })
    const domainId: string = event?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/calendar/'
    const linkName = `${i18next.t('eventCreated')} <a href="${link}">${eventName}</a>`
    const answer: string = linkName + '\n\n' + `${i18next.t('startDateEvent')} ${startDate}` + '\n' + `${i18next.t('endDateEvent')} ${endDate}`
    if (AIswitcher === 'off') {
      ctx.editMessageText(answer, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        ...Menu()
      })
    }
    if (AIswitcher === 'on') {
      ctx.reply(answer, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        ...Menu()
      })
    }
    console.log(data)
  } catch (error) {
    ctx.reply(i18next.t('errorSetEvent', { error }), Menu())
    console.log(error)
  }
}
