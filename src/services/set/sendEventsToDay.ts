import { type Telegraf } from 'telegraf'
// import { getEventsToDay } from './getEventsToDay'
import i18next from '../../localization/phrasesRus'
import { getEventsList } from '../get/getEventsList'
import { longMessageToMany } from '../../utils/utils'

export async function sendEventsToDay (bot: Telegraf, chatID: string, subChatID?: string): Promise<void> {
// const currentPage: number = 1
// const flag: string = Events.sendEvents
  const events: string = await getEventsList(bot)
  if (events.length === 0) {
    bot.telegram.sendMessage(chatID, i18next.t('ErrorGetEvents'), {
      message_thread_id: subChatID ? parseInt(subChatID) : undefined
    })
  } else {
    const message = longMessageToMany(events)

    for (let i = 0; i < message.length; i++) {
      await bot.telegram.sendMessage(chatID, message[i], {
        parse_mode: 'HTML',
        message_thread_id: subChatID ? parseInt(subChatID) : undefined
      })
    }
  }
}
