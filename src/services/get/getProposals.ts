import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollProposalsButtons } from '../../buttons/markups'
import { type proposalType } from '../../types/proposals'

export async function getProposals (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
      query{
        getProposals{
          id,
          name,
          authorUser{
            name
          },
          status,
          domain{
            id,
            name
          },
          createdAt,
          updatedAt,
        }
      }  
      `
      const data: { getProposals: proposalType[] } = await client.request(query)
      const proposals: proposalType[] = data.getProposals
      proposals.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (inputDate: string): string {
        const date = moment(inputDate)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const statuses = {
        new: 'Новое',
        accepted: 'Принято',
        holded: 'Отложено',
        rejected: 'Отклонено',
        ceased: 'Утратило силу',
        review: 'Требуется пересмотр'
      }
      const newLine: string = '\n'
      let message: string = i18next.t('proposals', { newLine })
      const totalPages = Math.ceil(proposals.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedProposals = proposals.slice(startIndex, startIndex + tasksPerPage)

      slicedProposals.forEach((proposal) => {
        const proposalAuthor: string = proposal.authorUser !== null ? proposal.authorUser.name : i18next.t('notAuthor')
        const dateText: string = formatDate(proposal.createdAt)
        const updateText: string = proposal.updatedAt !== null ? formatDate(proposal.updatedAt) : dateText
        const status: string = statuses[proposal.status.toString() as keyof typeof statuses]
        message += i18next.t('getProposals', { proposal, dateText, updateText, newLine, proposalAuthor, status })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollProposalsButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetProposals'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
