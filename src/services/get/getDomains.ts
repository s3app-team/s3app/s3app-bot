import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import config from '../../config/config'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons } from '../../buttons/markups'
import { type DomainsType } from '../../types/domains'
import { longMessageToMany } from '../../utils/utils'

export async function getAllDomains (ctx: Context): Promise<any> {
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()

      const query: string = gql`
          query GetDomains {
            getDomains {
              id
              name
              domainType
              participants {
                name
              }
              subDomains {
                id
                name
                domainType
                participants{
                  name
                }
              }
            }
          }`

      const data = await (client).request <{
        getDomains: DomainsType[]
      }>(query)

      const domains: DomainsType[] = data.getDomains

      const buildTree = (domains: DomainsType[]): DomainsType[] => {
        const organizations = domains.filter((domain) => domain.domainType === 'organization')
        const createNode = (domain: DomainsType): DomainsType => {
          const node: DomainsType = {
            id: domain.id,
            name: domain.name,
            participants: domain.participants,
            subDomains: domain.subDomains,
            domainType: domain.domainType
          }
          if (Array.isArray(domain.subDomains)) {
            domain.subDomains?.forEach((subDomain) => {
              const matchedSubDomain = organizations.find(
                (org) => org.name === subDomain.name
              )

              if (matchedSubDomain !== undefined) {
                node.subDomains?.push(createNode(matchedSubDomain))
              }
            })
          }

          return node
        }
        const rootOrganizations = organizations.filter((org) => org.domainType)

        const tree = rootOrganizations.map((rootOrg: DomainsType) => {
          return createNode(rootOrg)
        })
        return tree
      }

      const domainsTree = buildTree(domains)
      const newLine: string = '\n'

      const buildTreeMessage = (node: DomainsType, depth = 0): string => {
        let message = `${' '.repeat(depth * 3)}${depth === 0 ? i18next.t('domainsName') : i18next.t('subDomain')}: <a href="${config.s3appLink}/domains/${node.id}">${node.name}</a>\n`
        if (node.participants && node.participants.length > 0) {
          message += `${' '.repeat((depth) * 3)}${i18next.t('participants', { newLine })}`
          node.participants.forEach((participant) => {
            message += `${' '.repeat((depth + 2) * 3)}${participant.name}\n`
          })
        }
        message += '\n'
        if (node.subDomains && node.subDomains.length > 0) {
          message += `${' '.repeat((depth + 1) * 3)}   \n`
          node.subDomains.forEach((subDomain) => {
            message += buildTreeMessage(subDomain, depth + 2)
          })
          message += i18next.t('objectBoard')
        }
        return message
      }

      let domainMessage = i18next.t('rootDomains', { newLine })

      domainsTree.forEach((rootOrg: DomainsType) => {
        domainMessage += buildTreeMessage(rootOrg)
      })
      const messages = longMessageToMany(domainMessage)
      for (let i = 0; i < messages.length; i++) {
        await ctx.editMessageText(messages[i], {
          link_preview_options: {
            is_disabled: true
          },
          reply_markup: {
            inline_keyboard: [
              [{ text: i18next.t('back'), callback_data: 'back' }]
            ]
          },
          parse_mode: 'HTML'
        }
        )
      }
      // ctx.reply('Меню:', menuButtons())
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('errorGetDomains'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
