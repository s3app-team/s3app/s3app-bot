import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { type DomainsType } from '../../types/domains'
import { circleButtons, domainButtons } from '../../buttons/markups'

export async function getCircleList (ctx: Context, flag: string): Promise<void> {
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()

      const query: string = gql`
          query GetDomains {
            getDomains {
              id
              name
              domainType
              participants {
                name
              }
              subDomains {
                id
                name
                domainType
                participants{
                  name
                }
              }
            }
          }`
      const data = await (client).request <{
        getDomains: DomainsType[]
      }>(query)
      const domains: DomainsType[] = data.getDomains
      if (flag === 'get') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'domain:' + domain.id
          }]
        )
        )
        ctx.editMessageText(i18next.t('getCircleIssues'), circleButtons(domainData))
      } else if (flag === 'add_issue') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'setDomain:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      } else if (flag === 'add_event') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'add_event:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      } else if (flag === 'add_thought') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'add_thought:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      } else if (flag === 'add_tension') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'add_tension:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      } else if (flag === 'add_driver') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'add_driver:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      } else if (flag === 'add_proposal') {
        const domainData = domains.map((domain) => (
          [{
            text: domain.name,
            callback_data: 'add_proposal:' + domain.id
          }]
        )
        )
        ctx.reply(i18next.t('howDomain'), domainButtons(domainData))
      }
    } catch (error) {
      console.error(error)
    }
  }
}
