import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollDriversButtons } from '../../buttons/markups'
import { type driverType } from '../../types/drivers'

export async function getDrivers (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
      query{
        getDrivers{
         id,
          name,
          authorUser{
            name
          },
          driverEffect,
          driverState,
          domain{
            id,
            name
          },
          createdAt,
          updatedAt,
          }
      }  
      `
      const data: { getDrivers: driverType[] } = await client.request(query)
      const drivers: driverType[] = data.getDrivers
      drivers.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (inputDate: string): string {
        const date = moment(inputDate)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const statuses = {
        need: 'Потребность',
        goal: 'Цель',
        satisfied: 'Удовлетворен',
        irrelevant: 'Неактуален'
      }
      const newLine: string = '\n'
      let message: string = i18next.t('drivers', { newLine })
      const totalPages = Math.ceil(drivers.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedDrivers = drivers.slice(startIndex, startIndex + tasksPerPage)

      slicedDrivers.forEach((driver) => {
        const driverAuthor: string = driver.authorUser !== null ? driver.authorUser.name : i18next.t('notAuthor')
        const dateText: string = formatDate(driver.createdAt)
        const updateText: string = driver.updatedAt !== null ? formatDate(driver.updatedAt) : dateText
        const status: string = statuses[driver.driverState.toString() as keyof typeof statuses]
        message += i18next.t('getDrivers', { driver, dateText, updateText, newLine, driverAuthor, status })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollDriversButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetDrivers'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
