import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollButtonsDomainIssues } from '../../buttons/markups'
import { type DomainIssuesType } from '../../types/domains'
import moment from 'moment'
import config from '../../config/config'
import { type issueType } from '../../types/issues'

export async function getCircleIssues (domainId: string, currentPage: string | number, ctx: Context): Promise<void> {
  currentPage = +currentPage
  const domainID = domainId.toString()
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query ($domainId: ID!) {
        getDomain(id: $domainId) {
          id
          name
          issues {
            id
            name
            createdAt
            domain {
              id
              name
            }
            author {
              name
            }
            executor {
              name
            }
          }
        }
        }`

      const data = await client.request<{
        getDomain: DomainIssuesType
      }>(query, { domainId })
      const domain: DomainIssuesType = data.getDomain
      const issues: issueType[] = domain.issues
      issues.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (createdAt: string): string {
        const date = moment(createdAt)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')

        return formattedDate
      }

      const newLine: string = '\n'
      let message: string = ''
      let domainName: string = ''
      const tasksPerPage: number = 4
      const totalPages = Math.ceil(issues.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedIssues = issues.slice(startIndex, startIndex + tasksPerPage)

      slicedIssues.forEach((issue) => {
        const link = config.s3appLink + '/domains/' + issue.domain.id + '/issues/' + issue.id
        const domainLink = config.s3appLink + '/domains/' + issue.domain.id
        const issueAuthor: string = issue.author !== null ? issue.author.name : i18next.t('notAuthor')
        const issueExecutor: string = issue.executor !== null ? issue.executor.name : i18next.t('notExecutor')
        const dateText: string = formatDate(issue.createdAt)
        message += `<a href="${link}">${issue.name}</a>`
        message += i18next.t('domainIssues', { issue, newLine, issueAuthor, dateText, issueExecutor })
        domainName = `${i18next.t('issuesOfCircle')} <a href="${domainLink}">${issue.domain.name}</a>\n\n`
      })

      if (message !== '') {
        message = domainName + message
        message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })
        await ctx.editMessageText(message, {
          link_preview_options: {
            is_disabled: true
          },
          parse_mode: 'HTML',
          reply_markup: {
            inline_keyboard: [scrollButtonsDomainIssues(currentPage, totalPages, domainID)]
          }
        })
      } else {
        ctx.editMessageText(i18next.t('notCircleIssues'), menuButtons())
      }
    } catch {
      ctx.editMessageText(i18next.t('notCircleIssues'), menuButtons())
    }
  }
}
