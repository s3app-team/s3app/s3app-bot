import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollSprintsButtons } from '../../buttons/markups'
import { type sprintType } from '../../types/sprints'

export async function getSprints (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
      query{
        getSprints{
          id
          name
          goal
          startDate
          endDate
          participants{
              name
          }
          issues{
            id
            name
          }
          domain{
            id
            name
          }
        }
      }`
      const data: { getSprints: sprintType[] } = await client.request(query)
      const sprints: sprintType[] = data.getSprints
      function formatDate (Date: string): string {
        const date = moment(Date)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const newLine: string = '\n'
      let message: string = i18next.t('sprints', { newLine })
      const totalPages = Math.ceil(sprints.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedSprints = sprints.slice(startIndex, startIndex + tasksPerPage)

      slicedSprints.forEach((sprint) => {
        const startTime: string = formatDate(sprint.startDate)
        const endTime: string = formatDate(sprint.endDate)
        const participants: string = sprint.participants && sprint.participants.length > 0 ? sprint.participants.map(participant => participant.name).join(', ') : i18next.t('notParticipants')
        const issues: string = sprint.issues && sprint.issues.length > 0 ? sprint.issues.map(issue => issue.name).join('\n-->') : i18next.t('notIssues')

        message += i18next.t('GetSprints', { sprint, newLine, startTime, endTime, participants, issues })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollSprintsButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetSprints'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
