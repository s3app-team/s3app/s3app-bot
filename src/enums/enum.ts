export enum Domains {
  getDomains = 'get',
  setDomains = 'set'
}

export enum Priority {
  HighPriority = 2,
  MediumPriority = 1,
  LowPriority = 0
}

export enum ActionType {
  addIssue = 'add_issue',
  addEvent = 'add_event',
  addThought = 'add_thought',
  addTension = 'add_tension',
  addDriver = 'add_driver',
  addProposal = 'add_proposal',
  choice = 'choice',
  noAction = 'clear_session',
  back = 'back',
  backText = 'back_text',
  addAIRequest = 'add_ai_request',
  addAIDateFromUser = 'add_ai_date'
}

export enum Events {
  getEvents = 'get_events_now',
  sendEvents = 'regular_sending_of_events_for_to_day'
}

export enum IssueSearch {
  firstPage = 'first_page',
  currentPage = 'current_page'
}

export enum ProposalStatus {
  new = 'new',
  accepted = 'accepted',
  holded = 'holded',
  rejected = 'rejected',
  ceased = 'ceased',
  review = 'review',
  default = ''
}

export enum DriverState {
  need = 'need',
  goal = 'goal',
  satisfied = 'satisfied',
  irrelevant = 'irrelevant',
  defult = ''
}

export enum Commands {
  start = '/start',
  addIssue = '/add_issue',
  addEvent = '/add_event',
  issueSearch = '/issue_search',
  addThought = '/add_thought',
  addTension = '/add_tension',
  addDriver = '/add_driver',
  addProposal = '/add_proposal'
}

export enum Grade {
  positive = '+',
  negative = '-',
  neutral = '0'
}
