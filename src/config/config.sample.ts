export interface Config {
  serverURL: string
  serverToken: string
  telegramToken: string
  s3appLink: string
  allowedChatIds: string[]
  isDemo: boolean
  chatID: string
  subChatID?: string
  todayEventsTime?: string
  timeZone?: string
  gigaChatApiKey: string
  certPath?: string
  defaultDomainId: string
}

const config: Config = {
  serverURL: '',
  serverToken: '',
  telegramToken: '',
  s3appLink: '',
  allowedChatIds: ['your_chat_id_list_of_number_type'],
  isDemo: false,
  chatID: 'your_main_chat_id_for_sending',
  subChatID: 'your_subchat_id_for_sending',
  todayEventsTime: '4 0 * * *',
  timeZone: 'your_time_zone',
  gigaChatApiKey: 'your_Authorization_key_from_developers.sber.ru',
  certPath: 'path_to_file_with_сertificate',
  defaultDomainId: 'your_default_domain_id'
}

export default config
