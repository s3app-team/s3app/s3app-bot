import { type Config } from './config.sample'

const config: Config = {
  serverURL: process.env.S3APP_BOT_SERVER_URL ?? '',
  serverToken: process.env.S3APP_BOT_SERVER_TOKEN ?? '',
  telegramToken: process.env.S3APP_BOT_TELEGRAM_TOKEN ?? '',
  s3appLink: process.env.S3APP_BOT_S3APP_LINK ?? '',
  allowedChatIds: process.env.S3APP_BOT_ALLOWED_CHAT_IDS?.split(',') ?? [],
  isDemo: !!process.env.S3APP_BOT_IS_DEMO,
  chatID: process.env.S3APP_BOT_CHAT_ID ?? '',
  subChatID: process.env.S3APP_BOT_SUB_CHAT_ID ?? '',
  todayEventsTime: process.env.S3APP_BOT_TODAY_EVENTS_TIME ?? '4 0 * * *',
  timeZone: process.env.S3APP_BOT_TIME_ZONE ?? 'Europe/Moscow',
  gigaChatApiKey: process.env.S3APP_BOT_GIGA_CHAT_API_KEY ?? '',
  certPath: process.env.S3APP_BOT_CERT_PATH ?? '',
  defaultDomainId: process.env.S3APP_BOT_DEFAULT_DOMAIN_ID ?? ''
}

export default config
