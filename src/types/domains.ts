import { type issueType } from './issues'

export interface DomainsType {
  id: string
  name: string
  domainType: string
  participants?: Array<{
    name: string
  }>
  subDomains?: Array<{
    id: string
    name: string
    domainType: string
    participants?: Array<{
      name: string
    }>
  }>
  issues?: Array<{
    id: string
    name: string
    createdAt: string
    author: Array<{
      name: string
    }>
    executor: Array<{
      name: string
    }>
  }>
}

export interface DomainIssuesType {
  id: string
  name: string
  issues: issueType[]
}
