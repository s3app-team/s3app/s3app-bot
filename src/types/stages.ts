export interface DomainStagesType {
  id: string
  name: string
  stages: StageType[]
}

export interface StageType {
  id: string
  name: string
}
