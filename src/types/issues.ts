export interface issueType {
  id: string
  domain: {
    id: string
    name: string
  }
  createdAt: string
  name: string
  author: {
    name: string
  } | null
  executor: {
    name: string
  } | null
}
