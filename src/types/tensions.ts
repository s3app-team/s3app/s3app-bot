import { type ScalarTypeDefinitionNode } from 'graphql'

export interface tensionType {
  id: string
  domain: {
    id: string
    name: string
  }
  name: string
  authorUser: {
    name: string
  } | null
  createdAt: string
  updatedAt: string | null
  tensionMark: ScalarTypeDefinitionNode
}

export interface UserSessionTension {
  action: string
  tensionName: string
  tensionMark: string
  description: string
  domainId: string
}
