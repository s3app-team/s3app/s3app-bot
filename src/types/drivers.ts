import { type ScalarTypeDefinitionNode } from 'graphql'
import { type DriverState } from '../enums/enum'

export interface driverType {
  id: string
  name: string
  authorUser: {
    name: string
  } | null
  driverEffect: ScalarTypeDefinitionNode
  driverState: DriverState
  domain: {
    id: string
    name: string
  }
  createdAt: string
  updatedAt: string | null
}

export interface UserSessionDriver {
  action: string
  driverName: string
  description: string
  driverEffect: string
  driverState: DriverState
  domainId: string
}
