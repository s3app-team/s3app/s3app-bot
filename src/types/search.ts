export interface UserSession {
  chatId: number | undefined
  messageId: number | undefined
}
