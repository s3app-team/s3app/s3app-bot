export interface GigaChatResponse {
  choices: any
  response: string
}

export interface UserSessionAI {
  action: string
  requestText: string
}
